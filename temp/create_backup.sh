#!/usr/bin/bash

function createBackup {

JIRA_DB_NAME=jira_db
BACKUP_DIR1=/home/jira_backup/
BACKUP_DB_NAME=jira_db
BACKUP_DATE=$(date +%Y%m%d%H%M%S)
SQL_BACKUP_FILE=${BACKUP_DIR1}${BACKUP_DB_NAME}-${BACKUP_DATE}.sql
JIRA_HOME=/opt/atlassian/jira
BACKUP_ARCHIVE_NAME=jira_backup-${BACKUP_DATE}.tar.gz
SCP_BACKUP_DIR=/home/jira_backup
JIRA_DATA_PATH=/var/atlassian/application-data/jira

/usr/bin/expect <(cat << EOF
spawn mysqldump -uroot -p ${JIRA_DB_NAME} --result-file=${SQL_BACKUP_FILE}
expect "*password*"
send "password\r"
interact
EOF
)

if [ ! -d "${BACKUP_DIR1}" ];then
    mkdir -p ${BACKUP_DIR1}
fi

if [ ! -d "${JIRA_HOME}/logs" ];then
    echo -e "Can't find logs dir in ${JIRA_HOME}/";
    exit 1;
fi

cd ${BACKUP_DIR1}
tar -cvf ${BACKUP_ARCHIVE_NAME} ${BACKUP_DB_NAME}-${BACKUP_DATE}.sql
cd ${JIRA_HOME}/
tar -rvf ${BACKUP_DIR1}${BACKUP_ARCHIVE_NAME} logs
rm -rf ${SQL_BACKUP_FILE}
echo -e "${BACKUP_DIR1}${BACKUP_ARCHIVE_NAME}"

cd ${JIRA_DATA_PATH}
tar -rvf ${BACKUP_DIR1}${BACKUP_ARCHIVE_NAME} data

/usr/bin/expect <(cat << EOF
set timeout 120
spawn scp ${BACKUP_DIR1}${BACKUP_ARCHIVE_NAME} root@192.168.42.180:${SCP_BACKUP_DIR}
expect "*password*"
send "password\r"
interact
EOF
)

}

createBackup