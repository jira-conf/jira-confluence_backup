BACKUP_DIR="\/home\/conf_backup"
BACKUP_DIR_CLEAR=$(echo $BACKUP_DIR | sed -e 's/\\//g')
echo ${BACKUP_DIR} " :  "${BACKUP_DIR_CLEAR}
BACKUP_NAME=conf_backup
CONF_SOFTWARE_DATA=/var/atlassian/application-data/confluence
#CONF_DATA_DIR=/var/atlassian/application-data/confleunce/
CONF_DATA_ATTACHMENTS=${CONF_SOFTWARE_DATA}/attachments
CONF_HOME=/opt/atlassian/confluence
CONF_CFG_NAME=confluence.cfg.xml
PWD=$(pwd)
if [ ! -d "${BACKUP_DIR_CLEAR}" ];then
    echo -e "Backup dir(${BACKUP_DIR_CLEAR}) doen't exist!";
    exit 1;
else
    ARCH_NUMBER=$(ls -lR ${BACKUP_DIR_CLEAR}/*.tar.gz | wc -l)
    if [ "${ARCH_NUMBER}" = "0" ];then
	echo -e "Can't find any \".tar.gz\" files in ${BACKUP_DIR_CLEAR} directory"
	exit 1
    else
	find ${BACKUP_DIR_CLEAR} -type f ! -name '*.tar.gz' -delete
	rm -R -- ${BACKUP_DIR_CLEAR}/*/
    fi
fi


for entry in "${BACKUP_DIR_CLEAR}"/*
do
    #echo ${BACKUP_DIR}/${BACKUP_NAME}
    LAST=$(echo $entry | sed -e "s/${BACKUP_DIR}\/${BACKUP_NAME}-//g")
    IFS=' ' read -r -a array <<< $(echo $LAST | sed -e "s/.tar.gz//g")
done
max=${array[0]}
#ROOT='root'
#ROOT_PASS='password'
CONF_DB_NAME='conf_db'
CONF_DB_USERNAME='confdbuser'
CONF_DB_USER_PASS='password'
CONF_USER_HOST='localhost'

function dbRecovery(){
    function import_db(){

	su - postgres -c "psql -c \"DROP DATABASE IF EXISTS ${CONF_DB_NAME};\""
	su - postgres -c "psql -c \"REASSIGN OWNED BY ${CONF_DB_USERNAME} TO postgres;\""
	su - postgres -c "psql -c \"DROP OWNED BY ${CONF_DB_USERNAME};\""
	su - postgres -c "psql -c \"REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM ${CONF_DB_USERNAME};\""
	su - postgres -c "psql -c \"REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM ${CONF_DB_USERNAME};\""
	su - postgres -c "psql -c \"REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM ${CONF_DB_USERNAME};\""
	su - postgres -c "psql -c \"DROP USER ${CONF_DB_USERNAME};\""
#	su - postgres -c "psql -c \"DROP DATABASE IF EXISTS ${CONF_DB_NAME};\""
	su - postgres -c "psql -c \"CREATE USER ${CONF_DB_USERNAME} WITH ENCRYPTED PASSWORD '${CONF_DB_USER_PASS}';\"" 2>/dev/null
	su - postgres -c "psql -c \"CREATE DATABASE ${CONF_DB_NAME};\""
	su - postgres -c "psql -c \"GRANT ALL PRIVILEGES ON DATABASE ${CONF_DB_NAME} TO ${CONF_DB_USERNAME};\""

	su - postgres -c "psql ${CONF_DB_NAME} < ${PWD}/$1"

    }

for element in "${array[@]}"
do
    if [ $max -lt $element ];then
	echo "MAX IS $element"
	max=$element
    fi
    if [ "$element" = "${array[${#array[@]}-1]}" ];then
	tar -xvf "${BACKUP_DIR_CLEAR}/${BACKUP_NAME}-${element}.tar.gz"
	SQL_FILE_NAME="conf_db-${max}.sql"
	if [ -d "${CONF_HOME}/logs" ];then
	    cp -Rf logs/* ${CONF_HOME}/logs
	else
	    echo -e "There is no Confluence's log directory in ${CONF_HOME}"
	fi
	if [ -d "${CONF_DATA_ATTACHMENTS}" ];then
	    rm -r ${CONF_DATA_ATTACHMENTS}/* > /dev/null 2>&1
	    cp -R attachments/* ${CONF_DATA_ATTACHMENTS}
	    echo -e "Attachments from backup were copied to attachments directory!"
	else
    	    echo -e "There is no Confluence's attachments directory in ${CONF_DATA_DATA}"
	    mkdir -p ${CONF_DATA_ATTACHMENTS}
	    cp -R attachments/* ${CONF_DATA_ATTACHMENTS}
	    echo -e "Attachments directory was created and filled with backup's attachments"
	fi
	chown -R confluence:confluence ${CONF_DATA_ATTACHMENTS}/
	if [ -f "${CONF_SOFTWARE_DATA}/${CONF_CFG_NAME}" ];then
	    rm -f ${CONF_SOFTWARE_DATA}/${CONF_CFG_NAME}
	    cp ${CONF_CFG_NAME} ${CONF_SOFTWARE_DATA}
	    echo -e "Confluence config file was founded and replaced in ${CONF_SOFTWARE_DATA} directory!"
	else
	    cp ${CONF_CFG_NAME} ${CONF_SOFTWARE_DATA}
	    echo -e "Confluence config file was copied to ${CONF_SOFTWARE_DATA} directory"
	fi
	chown -R confluence:confluence ${CONF_SOFTWARE_DATA}/${CONF_CFG_NAME}
	import_db ${SQL_FILE_NAME}
	rm -f ${SQL_FILE_NAME}
    fi
done
rm -rf logs
rm -rf attachments
rm -rf ${CONF_CFG_NAME}
systemctl restart confluence
}
dbRecovery