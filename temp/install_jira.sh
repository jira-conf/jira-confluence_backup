#!/bin/bash

JIRA_INSTALL_PATH="/opt/atlassian/jira"
JIRA_SOFTWARE_DATA="/var/atlassian/application-data/jira"
CONNECTOR_NAME="mysql-connector.tar.gz"
TEMP_DIR="temp_conn_dir"
JIRA_VER="8.13.8"
JIRA_NAME="jira-${JIRA_VER}.bin"

function removeJira{

if [ "$1" = "/d" ];then
    systemctl stop jira
    ${JIRA_INSTALL_PATH}/uninstall
    if [ -d "${JIRA_INSTALL_PATH}/logs" ];then rm -rf ${JIRA_INSTALL_PATH}/logs;fi;
    if [ -d "${JIRA_INSTALL_PATH}/lib" ];then rm -rf ${JIRA_INSTALL_PATH}/lib;fi;
    rm -rf ${JIRA_SOFTWARE_DATA}
    echo -e "${INF}Jira was removed!"
    exit 1
fi

}

function setWGETproxy{

WGETRC="/etc/wgetrc"
PROXY="http:\/\/user1234:lab233234@192\.168\.1\.234:3113\/"
STR1="#https_proxy = http:\/\/proxy\.yoyodyne\.com:18023\/"
STR2="#http_proxy = http:\/\/proxy\.yoyodyne\.com:18023\/"
STR3="#use_proxy = on"
EXT1="http_proxy = http://user1234:lab233234@192.168.1.234:3113/"
EXT2="https_proxy = http://user1234:lab233234@192.168.1.234:3113/"
if grep -Fxq "$EXT1" "${WGETRC}"
then
echo "${INF}FOUND ${EXT1}!";
else
sed -i "s/${STR2}/http_proxy = ${PROXY}/" "${WGETRC}"
fi
if grep -Fxq "$EXT2" "${WGETRC}"
then
echo "${INF}FOUND ${EXT2}!";
else
sed -i "s/${STR1}/https_proxy = ${PROXY}/" "${WGETRC}"
fi
if grep -Fxq "use_proxy = on" "${WGETRC}"
then
echo "${INF}FOUND use_proxy = on!";
else
sed -i "s/${STR3}/use_proxy = on/" "${WGETRC}"
fi


}

function installJira{

if [ ! -f "${JIRA_NAME}" ];then
    wget -O ${JIRA_NAME} https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-${JIRA_VER}-x64.bin
else
    echo -e "${INF}Installer of JIRA (${JIRA_NAME}) was founded in current folder ${GREEN}$(pwd)${NC}"
    echo -e "${INF}Jira will be installed from this file (${GREEN}${JIRA_NAME}${NC})!"
fi
chmod a+x ${JIRA_NAME}

setWGETproxy

echo -e "${INF}Stating to install Jira..."
echo -e "o\n2\n${JIRA_INSTALL_PATH}\n${JIRA_SOFTWARE_DATA}\n1\ny\ni" | ./${JIRA_NAME}
FILE="${JIRA_INSTALL_PATH}/bin/setenv.sh"
sed -i 's/JVM_SUPPORT_RECOMMENDED_ARGS=\"\"/JVM_SUPPORT_RECOMMENDED_ARGS=\"-Dhttps.proxyHost=192.168.1.234 -Dhttps.proxyPort=3113 -Dhttp.nonProxyHost=localhost\\|192.168.* -Dhttps.proxyUser=user1234 -Dhttps.proxyPassword=lab233234\"/' $FILE

wget -O ${CONNECTOR_NAME} https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-8.0.26.tar.gz
if [ ! -d "${TEMP_DIR}" ];then
    mkdir ${TEMP_DIR};
elif [ -d ${TEMP_DIR} ];then
    rm -rf ${TEMP_DIR}
    mkdir ${TEMP_DIR}
fi

tar -xvf ${CONNECTOR_NAME} -C ${TEMP_DIR}
set -- ${TEMP_DIR}/*
DIR_NAME=$1
ARCH_DIR_NAME=$(echo ${DIR_NAME} | sed -E "s/^${TEMP_DIR}\///")
#echo ${ARCH_DIR_NAME}


cp ${TEMP_DIR}/${ARCH_DIR_NAME}/${ARCH_DIR_NAME}.jar ${JIRA_INSTALL_PATH}/lib
systemctl restart jira
#systemctl status jira

rm -rf ${TEMP_DIR}
rm -f ${CONNECTOR_NAME}

echo -e "${INF}Open this link in your browser: localhost:8080 "

}