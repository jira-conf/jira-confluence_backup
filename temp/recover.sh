BACKUP_DIR="\/home\/jira_backup"
BACKUP_DIR_CLEAR=$(echo $BACKUP_DIR | sed -e 's/\\//g')
echo "NEW CLEAR PATH: ${BACKUP_DIR_CLEAR}"
BACKUP_NAME=jira_backup
JIRA_DATA_DIR=/var/atlassian/application-data/jira/data
JIRA_HOME=/opt/atlassian/jira

if [ ! -d "${BACKUP_DIR_CLEAR}" ];then echo -e "Backup dir(${BACKUP_DIR_CLEAR}) doen't exist";exit 1;fi
for entry in "${BACKUP_DIR}"/*
do
	LAST=$(echo $entry | sed -e "s/${BACKUP_DIR}\/${BACKUP_NAME}-//g")
	IFS=' ' read -r -a array <<< $(echo $LAST | sed -e "s/.tar.gz//g")
done
max=${array[0]}
ROOT='root'
ROOT_PASS='password'
JIRA_DB_NAME='jira_db'
JIRA_DB_USERNAME='jiradbuser'
JIRA_DB_USER_PASS='password'
HOST='localhost'

function dbRecovery(){

function import_db(){
    systemctl start mysql
    MYSQL_CONF=/etc/mysql/mysql.conf.d/mysqld.cnf
    sed -i 's/sql_mode = NO_AUTO_VALUE_ON_ZERO//' ${MYSQL_CONF}
    sed -i 's/default-storage-engine=INNODB//' ${MYSQL_CONF}
    sed -i 's/character_set_server=utf8mb4//' ${MYSQL_CONF}
    sed -i 's/innodb_default_row_format=DYNAMIC//' ${MYSQL_CONF}
    sed -i 's/innodb_log_file_size=2G//' ${MYSQL_CONF}
    sed -i '/^\[mysqld\]/a default-storage-engine=INNODB\ncharacter_set_server=utf8mb4\ninnodb_default_row_format=DYNAMIC\ninnodb_log_file_size=2G' ${MYSQL_CONF}
    systemctl restart mysql

    mysql -u $ROOT -p$ROOT_PASS -Bse "INSTALL PLUGIN validate_password SONAME 'validate_password.so';" > /dev/null 2>&1
    mysql -u $ROOT -p$ROOT_PASS -Bse "\
    SET GLOBAL validate_password_policy = LOW;\
    SET GLOBAL validate_password_special_char_count = 0;\
    CREATE USER IF NOT EXISTS '${JIRA_DB_USERNAME}'@'${HOST}' IDENTIFIED BY '${JIRA_DB_USER_PASS}';\
    DROP DATABASE IF EXISTS ${JIRA_DB_NAME};\
    CREATE DATABASE ${JIRA_DB_NAME};\
    GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,REFERENCES,ALTER,INDEX on ${JIRA_DB_NAME}.* TO '${JIRA_DB_USERNAME}'@'${HOST}';\
    FLUSH PRIVILEGES;"
    mysql -u $USER -p${ROOT_PASS} "${JIRA_DB_NAME}" < "$1"
}
for element in "${array[@]}"
do
    if [ $max -lt $element ];then
	echo "MAX IS $element"
	max=$element
    fi
    if [ "$element" = "${array[${#array[@]}-1]}" ];then
	#rm -f *.sql
	tar -xvf "${BACKUP_DIR_CLEAR}/${BACKUP_NAME}-${element}.tar.gz"
	SQL_FILE_NAME="jira_db-${max}.sql"
	ls
	if [ -d "${JIRA_HOME}/logs" ];then
	    cp -Rf logs/* ${JIRA_HOME}/logs
	else
	    echo -e "There is no JIRA's log directory in ${JIRA_HOME}"
	fi
	if [ -d "${JIRA_DATA_DIR}" ];then
	    rm -r ${JIRA_DATA_DIR}/* > /dev/null 2>&1
	    cp -R data/* ${JIRA_DATA_DIR}
	    chown -R jira:jira ${JIRA_DATA_DIR}/
	else
	    echo -e "There is no JIRA's data directory in ${JIRA_DATA_DIR}"
	    #mkdir -p ${JIRA_DATA_DIR}
	    #cp data/* ${JIRA_DATA_DIR}
	fi
	import_db ${SQL_FILE_NAME}
	rm -f ${SQL_FILE_NAME}
    fi
done
rm -rf logs
rm -rf data

}
