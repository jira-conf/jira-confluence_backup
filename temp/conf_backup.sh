CONF_DB_NAME=conf_db
BACKUP_DIR="/home/conf_backup"
BACKUP_DB_NAME=conf_db
BACKUP_DATE=$(date +%Y%m%d%H%M%S)
SQL_BACKUP_FILE=${BACKUP_DIR}/${BACKUP_DB_NAME}-${BACKUP_DATE}.sql
CONF_HOME=/opt/atlassian/confluence
BACKUP_ARCHIVE_NAME=conf_backup-${BACKUP_DATE}.tar.gz
SCP_BACKUP_DIR=/home/conf_backup
CONF_SOFTWARE_DATA=/var/atlassian/application-data/confluence

function createBackup {
if [ ! -d "${BACKUP_DIR}" ];then
    install -d -m 0755 -o postgres -g postgres ${BACKUP_DIR}
else
    USER=$(stat -c '%U' ${BACKUP_DIR})
    GROUP=$(stat -c '%G' ${BACKUP_DIR})
    if [ "${USER}" != "postgres" ] || [ "${GROP}" != "postgres" ];then
	chown -R postgres:postgres ${BACKUP_DIR}
    fi
fi

if [ ! -d "${CONF_HOME}/logs" ];then
    echo -e "Can't find logs dir in ${CONF_HOME}";
#    exit 1;
fi
su - postgres -c "whoami;pg_dump conf_db > ${SQL_BACKUP_FILE}"

cd ${BACKUP_DIR}
tar -cvf ${BACKUP_ARCHIVE_NAME} ${BACKUP_DB_NAME}-${BACKUP_DATE}.sql
cd ${CONF_HOME}
tar -rvf ${BACKUP_DIR}/${BACKUP_ARCHIVE_NAME} logs

cd ${CONF_SOFTWARE_DATA}
tar -rvf ${BACKUP_DIR}/${BACKUP_ARCHIVE_NAME} attachments confluence.cfg.xml

rm ${SQL_BACKUP_FILE}

}