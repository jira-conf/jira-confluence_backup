#!/usr/bin/bash
CONF_INSTALL_PATH=/opt/atlassian/confluence
CONF_SOFTWARE_DATA=/var/atlassian/application-data/confluence
CONF_VER="7.13.0"
CONF_NAME="conf-${CONF_VER}.bin"
##############################temp vars
#ROOT='root'
#ROOT_PASS='password'
CONF_DB_NAME='conf_db'
CONF_DB_USERNAME='confdbuser'
CONF_DB_USER_PASS='password'
CONF_USER_HOST='localhost'
#############################

function rmCatalinaPid {
systemctl confluence start 2>/dev/null
RES=$(systemctl status confluence | grep 'Active: failed')
if [ "${RES}" != "" ];then
    killall -9 java
    rm -f ${CONF_HOME}work/catalina.pid
    systemctl restart confluence
fi
}
function cnfInstall {

if [ "$1" = "/d" ];then
    echo "HERE"
    systemctl stop confluence
    su - postgres -c "psql -c \"DROP DATABASE IF EXISTS ${CONF_DB_NAME};\""
    su - postgres -c "psql -c \"REASSIGN OWNED BY ${CONF_DB_USERNAME} TO postgres;\""
    su - postgres -c "psql -c \"DROP OWNED BY ${CONF_DB_USERNAME};\""
    su - postgres -c "psql -c \"REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM ${CONF_DB_USERNAME};\""
    su - postgres -c "psql -c \"REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM ${CONF_DB_USERNAME};\""
    su - postgres -c "psql -c \"REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM ${CONF_DB_USERNAME};\""
    su - postgres -c "psql -c \"DROP USER ${CONF_DB_USERNAME};\""
    echo -e "y" | ${CONF_INSTALL_PATH}/uninstall
    if [ -d "${CONF_INSTALL_PATH}/logs" ];then rm -rf ${CONF_INSTALL_PATH}/logs;fi
    rm -rf ${CONF_SOFTWARE_DATA}
    echo -e "Confluence was removed!"
    exit 1
fi
#exit 1
if [ ! -f "${CONF_NAME}" ];then
    wget -O ${CONF_NAME} https://www.atlassian.com/software/confluence/downloads/binary/atlassian-confluence-${CONF_VER}-x64.bin
else
    echo -e "Installer of Confluence (${CONF_NAME}) was founded in current folder $(pwd)"
    echo -e "Confluence will be installed from this file (${CONF_NAME})!"
fi
chmod a+x ${CONF_NAME}

echo -e "Stating to install Confluence..."
echo -e "o\n2\n${CONF_INSTALL_PATH}\n${CONF_SOFTWARE_DATA}\n1\ny\ny" | ./${CONF_NAME}
rmCatalinaPid
}
cnfInstall