#!/usr/bin/bash

PCOL='\033[1;35m'
BOLD='\033[1m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
RED='\033[1;31m'
NC='\033[0m'
CYAN='\033[0;36m'
ERR="${RED}[E]${NC} "
WAR="${YELLOW}[W]${NC} "
OK="${GREEN}[OK]${NC} "
INF="${CYAN}[I]${NC} "

###########INSTALL_VARS#################
CONF_INSTALL_PATH=/opt/atlassian/confluence
CONF_SOFTWARE_DATA=/var/atlassian/application-data/confluence
CONF_VER="7.13.0"
CONF_NAME="conf-${CONF_VER}.bin"

#############CREATE_BACKUP_VARS#################

function check_first_param() {
    if [ $# -eq 0 ];then
	echo -e "${ERR}There is no arguments! You must pass at least one"
	echo -e "${INF}Example of usage: ./conf_backup.sh h [d i r b]"
	exit 1
    else
	SIZE=${#1}
	if [ $SIZE -eq 14 ];then
		if [[ $1 =~ ^[0-9]+$ ]];then
		    echo -e "${INF}Archive file will be build with last friday's date in name\n"
		    BACKUP_DATE=$1
		else
		    echo -e "${ERR}First argument contains not only digits." 
		    exit 1
		fi
	else
	    BACKUP_DATE=$(date +%Y%m%d%H%M%S)
	fi
fi
}

check_first_param $1
CONF_DB_NAME=conf_db
BACKUP_DIR=/home/conf_backup
BACKUP_DB_NAME=conf_db
#BACKUP_DATE=$(date +%Y%m%d%H%M%S)
#SQL_BACKUP_FILE=${BACKUP_DIR}/${BACKUP_DB_NAME}-${BACKUP_DATE}.sql
BACKUP_ARCHIVE_NAME=conf_backup-${BACKUP_DATE}.tar.gz
SCP_BACKUP_DIR=/home/conf_backup
SCP_ADDRESS='192.168.42.180'
#SCP_PASS='password'
#############RECOVERY_VARS##################

BACKUP_DIR="\/home\/conf_backup"
BACKUP_DIR_CLEAR=$(echo $BACKUP_DIR | sed -e 's/\\//g')
SQL_BACKUP_FILE=${BACKUP_DIR_CLEAR}/${BACKUP_DB_NAME}-${BACKUP_DATE}.sql
BACKUP_NAME=conf_backup
CONF_DATA_ATTACHMENTS=${CONF_SOFTWARE_DATA}/attachments
CONF_HOME=/opt/atlassian/confluence
CONF_CFG_NAME=confluence.cfg.xml
PWD=$(pwd)

if [ ! -d "${BACKUP_DIR_CLEAR}" ];then
    echo -e "${ERR}Backup dir(${PCOL}${BACKUP_DIR_CLEAR}${NC}) doen't exist!";
    exit 1;
else
    ARCH_NUMBER=$(ls -lR ${BACKUP_DIR_CLEAR}/*.tar.gz | wc -l)
    if [ "${ARCH_NUMBER}" = "0" ];then
	echo -e "${ERR}Can't find any \".tar.gz\" files in ${PCOL}${BACKUP_DIR_CLEAR}${NC} directory"
	exit 1
    else
	find ${BACKUP_DIR_CLEAR} -type f ! -name '*.tar.gz' -delete
	rm -R -- ${BACKUP_DIR_CLEAR}/*/
    fi
fi


for entry in "${BACKUP_DIR}"/*
do
#    echo ${BACKUP_DIR}/${BACKUP_NAME}
    LAST=$(echo $entry | sed -e "s/${BACKUP_DIR}\/${BACKUP_NAME}-//g")
    IFS=' ' read -r -a array <<< $(echo $LAST | sed -e "s/.tar.gz//g")
done
max=${array[0]}
CONF_DB_NAME='conf_db'
CONF_DB_USERNAME='confdbuser'
CONF_DB_USER_PASS='password'

#############BACKUP_FUNCTION############

function createBackup {
if [ ! -d "${BACKUP_DIR_CLEAR}" ];then
    install -d -m 0755 -o postgres -g postgres ${BACKUP_DIR_CLEAR}
else
    USER=$(stat -c '%U' ${BACKUP_DIR_CLEAR})
    GROUP=$(stat -c '%G' ${BACKUP_DIR_CLEAR})
    if [ "${USER}" != "postgres" ] || [ "${GROP}" != "postgres" ];then
	chown -R postgres:postgres ${BACKUP_DIR_CLEAR}
    fi
fi

if [ ! -d "${CONF_HOME}/logs" ];then
    echo -e "${WAR}Can't find logs dir in ${PCOL}${CONF_HOME}${NC}";
#    exit 1;
fi
su - postgres -c "whoami;pg_dump conf_db > ${SQL_BACKUP_FILE}"

cd ${BACKUP_DIR_CLEAR}
tar -cvf ${BACKUP_ARCHIVE_NAME} ${BACKUP_DB_NAME}-${BACKUP_DATE}.sql
cd ${CONF_HOME}
tar -rvf ${BACKUP_DIR_CLEAR}/${BACKUP_ARCHIVE_NAME} logs

cd ${CONF_SOFTWARE_DATA}
tar -rvf ${BACKUP_DIR_CLEAR}/${BACKUP_ARCHIVE_NAME} attachments confluence.cfg.xml

rm ${SQL_BACKUP_FILE}


scp -i ~/.ssh/id_rsa_180 ${BACKUP_DIR_CLEAR}/${BACKUP_ARCHIVE_NAME} root@${SCP_ADDRESS}:${SCP_BACKUP_DIR}
#/usr/bin/expect <(cat << EOF
#set timeout 120
#spawn scp ${BACKUP_DIR_CLEAR}/${BACKUP_ARCHIVE_NAME} root@${SCP_ADDRESS}:${SCP_BACKUP_DIR}
#expect "*password*"
#send "${SCP_PASS}\r"
#interact
#EOF
#)


}

#####################BACKUP_FUNCTION_END########################
###################RECOVERY FUNCTION###############################

function rmCatalinaPid {

systemctl start confluence 2>/dev/null
RES=$(systemctl status confluence | grep 'Active: failed')
if [ "${RES}" != "" ];then
    killall -9 java
    rm -f ${CONF_HOME}/work/catalina.pid
    systemctl restart confluence
fi
}

function dbRecovery(){
    function import_db(){
	service postgresql restart
	su - postgres -c "psql -c \"DROP DATABASE IF EXISTS ${CONF_DB_NAME};\""
	su - postgres -c "psql -c \"REASSIGN OWNED BY ${CONF_DB_USERNAME} TO postgres;\""
	su - postgres -c "psql -c \"DROP OWNED BY ${CONF_DB_USERNAME};\""
	su - postgres -c "psql -c \"REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM ${CONF_DB_USERNAME};\""
	su - postgres -c "psql -c \"REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM ${CONF_DB_USERNAME};\""
	su - postgres -c "psql -c \"REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM ${CONF_DB_USERNAME};\""
	su - postgres -c "psql -c \"DROP USER ${CONF_DB_USERNAME};\""
	su - postgres -c "psql -c \"CREATE USER ${CONF_DB_USERNAME} WITH ENCRYPTED PASSWORD '${CONF_DB_USER_PASS}';\"" 2>/dev/null
	su - postgres -c "psql -c \"CREATE DATABASE ${CONF_DB_NAME};\""
	su - postgres -c "psql -c \"GRANT ALL PRIVILEGES ON DATABASE ${CONF_DB_NAME} TO ${CONF_DB_USERNAME};\""

	su - postgres -c "psql ${CONF_DB_NAME} < ${PWD}/$1"

    }

for element in "${array[@]}"
do
    if [ $max -lt $element ];then
	#echo "MAX IS $element"
	max=$element
    fi
    if [ "$element" = "${array[${#array[@]}-1]}" ];then
	tar -xvf "${BACKUP_DIR_CLEAR}/${BACKUP_NAME}-${element}.tar.gz"
	SQL_FILE_NAME="conf_db-${max}.sql"
	if [ -d "${CONF_HOME}/logs" ];then
	    cp -Rf logs/* ${CONF_HOME}/logs
	else
	    echo -e "${WAR}There is no Confluence's log directory in ${PCOL}${CONF_HOME}${NC}"
	fi
	if [ -d "${CONF_DATA_ATTACHMENTS}" ];then
	    rm -r ${CONF_DATA_ATTACHMENTS}/* > /dev/null 2>&1
	    cp -R attachments/* ${CONF_DATA_ATTACHMENTS}
	    echo -e "${OK}Attachments from backup were copied to attachments directory!"
	else
	    echo -e "${WAR}There is no Confluence's attachments directory in ${PCOL}${CONF_DATA_ATTACHMENTS}${NC}"
	    mkdir -p ${CONF_DATA_ATTACHMENTS}
	    cp -R attachments/* ${CONF_DATA_ATTACHMENTS}
	    echo -e "${OK}Attachments directory was created and filled with backup's attachments"
	fi
	chown -R confluence:confluence ${CONF_DATA_ATTACHMENTS}/
	if [ -f "${CONF_SOFTWARE_DATA}/${CONF_CFG_NAME}" ];then
	    rm -f ${CONF_SOFTWARE_DATA}/${CONF_CFG_NAME}
	cp ${CONF_CFG_NAME} ${CONF_SOFTWARE_DATA}
	    echo -e "${OK}Confluence config file was founded and replaced in ${PCOL}${CONF_SOFTWARE_DATA}${NC} directory!"
	else
	    cp ${CONF_CFG_NAME} ${CONF_SOFTWARE_DATA}
	    echo -e "${OK}Confluence config file was copied to ${PCOL}${CONF_SOFTWARE_DATA}${NC} directory"
	fi
	chown -R confluence:confluence ${CONF_SOFTWARE_DATA}/${CONF_CFG_NAME}
	import_db ${SQL_FILE_NAME}
	rm -f ${SQL_FILE_NAME}
    fi
done
rm -rf logs
rm -rf attachments
rm -rf ${CONF_CFG_NAME}
rmCatalinaPid
#systemctl restart confluence
}


#####################RECOVERY_FUNCTION_END########################
#####################REMOVE_FUNCTION#######################

function rmConf {
    systemctl stop confluence
    su - postgres -c "psql -c \"DROP DATABASE IF EXISTS ${CONF_DB_NAME};\""
    su - postgres -c "psql -c \"REASSIGN OWNED BY ${CONF_DB_USERNAME} TO postgres;\""
    su - postgres -c "psql -c \"DROP OWNED BY ${CONF_DB_USERNAME};\""
    su - postgres -c "psql -c \"REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM ${CONF_DB_USERNAME};\""
    su - postgres -c "psql -c \"REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM ${CONF_DB_USERNAME};\""
    su - postgres -c "psql -c \"REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM ${CONF_DB_USERNAME};\""
    su - postgres -c "psql -c \"DROP USER ${CONF_DB_USERNAME};\""
    echo -e "y" | ${CONF_INSTALL_PATH}/uninstall
    if [ -d "${CONF_INSTALL_PATH}/logs" ];then rm -rf ${CONF_INSTALL_PATH}/logs;fi
    rm -rf ${CONF_SOFTWARE_DATA}
    echo -e "${OK}Confluence was removed!"
}

#######################REMOVE_FUNCTION_END#######################
####################FUNCTION_WGET_PROXY#################

function setWGETproxy {

WGETRC="/etc/wgetrc"
PROXY="http:\/\/user1234:lab233234@192\.168\.1\.234:3113\/"
STR1="#https_proxy = http:\/\/proxy\.yoyodyne\.com:18023\/"
STR2="#http_proxy = http:\/\/proxy\.yoyodyne\.com:18023\/"
STR3="#use_proxy = on"
EXT1="http_proxy = http://user1234:lab233234@192.168.1.234:3113/"
EXT2="https_proxy = http://user1234:lab233234@192.168.1.234:3113/"
if grep -Fxq "$EXT1" "${WGETRC}"
then
echo -e "${OK}FOUND ${EXT1}!";
else
sed -i "s/${STR2}/http_proxy = ${PROXY}/" "${WGETRC}"
fi
if grep -Fxq "$EXT2" "${WGETRC}"
then
echo -e "${OK}FOUND ${EXT2}!";
else
sed -i "s/${STR1}/https_proxy = ${PROXY}/" "${WGETRC}"
fi
if grep -Fxq "use_proxy = on" "${WGETRC}"
then
echo -e "${OK}FOUND use_proxy = on!";
else
sed -i "s/${STR3}/use_proxy = on/" "${WGETRC}"
fi

}


#####################FUNCTION_WGET_PROXY_END################
#####################INSTALL_FUNCTION#######################

function cnfInstall {

setWGETproxy

if [ ! -f "${CONF_NAME}" ];then
    wget -O ${CONF_NAME} https://www.atlassian.com/software/confluence/downloads/binary/atlassian-confluence-${CONF_VER}-x64.bin
else
    echo -e "${OK}Installer of Confluence (${CONF_NAME}) was founded in current folder $(pwd)"
    echo -e "${INF}Confluence will be installed from this file (${CONF_NAME})!"
fi
chmod a+x ${CONF_NAME}

echo -e "${INF}Stating to install Confluence..."
echo -e "o\n2\n${CONF_INSTALL_PATH}\n${CONF_SOFTWARE_DATA}\n1\ny\ny" | ./${CONF_NAME}

rmCatalinaPid

}


#####################INSTALL_FUNCTION_END######################

h_flag=0
i_flag=0
#d_flag=0

TMP_FILE=/tmp/tmp_file

if [ ! -f "${TMP_FILE}" ];then touch "${TMP_FILE}";else rm -f ${TMP_FILE};touch "${TMP_FILE}";fi;

for var in "$@"
do
#    echo "VAR: "$var
    if [ "$var" = "h" ];then echo "h_flag=1" >> "${TMP_FILE}";fi
    if [ "$var" = "i" ];then echo "i_flag=1" >> "${TMP_FILE}";fi
#    if [ "$var" = "d" ];then echo "d_flag=1" >> "${TMP_FILE}";fi

done

h_flag=$(echo $(sed -n "/^h_flag=*/p" "${TMP_FILE}") | sed "s/^h_flag=//")
i_flag=$(echo $(sed -n "/^i_flag=*/p" "${TMP_FILE}") | sed "s/^i_flag=//")
#d_flag=$(echo $(sed -n "/^d_flag=*/p" "${TMP_FILE}") | sed "s/^d_flag=//")

if [ "${h_flag}" = "" ];then h_flag=0;fi
if [ "${i_flag}" = "" ];then i_flag=0;fi
#if [ "${d_flag}" = "" ];then d_flag=0;fi


function h_check(){
    #echo $1
    if [ "$1" = "1" ];then echo -e "${WAR}h flag must be used without others flags ";exit 1;fi;
}

for var in "$@"
do
    
    if [ "$var" = "h" ];then
	
	echo -e "\
	${INF}List of flags:\n\
	${INF}${BOLD}d${NC} - remove Confluence;\n\
	${INF}${BOLD}i${NC} - install Confluence;\n\
	${INF}${BOLD}r${NC} - recovery Confluence data from backup;\n\
	${INF}${BOLD}b${NC} - backup Confluence data. Backup includes next:\n\
	\t${PCOL}/opt/atlassian/confluence/logs${NC},\n\
	\t${PCOL}/var/atlassian/application-data/confluence/attachments${NC},\n\
	\t${PCOL}/var/atlassian/application-data/confluence/confluence.cfg.xml${NC},\n\
	\tcurrent Confluence database."
	h_flag=1
    fi
    if [ "$var" = "d" ];then
	h_check $h_flag
	echo -e "${INF}Removing Confluence..."
	#d_flag=1
	#remove
	rmConf
	
    fi
    if [ "$var" = "i" ];then
	h_check $h_flag
	echo -e "${INF}Installing Confluence..."
	i_flag=1
	#install
	cnfInstall
	
    fi
    if [ "$var" = "r" ];then
	h_check $h_flag
	echo -e "${INF}Recovering Confluence data..."
	#recovery
	dbRecovery
    fi
    if [ "$var" = "b" ];then
	h_check $h_flag
	if [ "${i_flag}" = "1" ];then
	    echo -e "${ERR}There is no reason to create empty backup!\n${ERR}Don't use b flag with 'i' flag";
	    exit 1
	fi
	echo -e "${INF}Creating backup..."
	#backup
	createBackup
	
    fi
    #echo "$var"
    
done