<a name="content">***Содержание***</a>  
[1) Подготовительный этап](#preparing)  
[1.1) Установка mysql для Jira](#mysql-installing)  
[1.2) Установка PostgreSQL для Confluence](#postgresql-installing)  
[2) Установка Jira](#jira-installing)  
[2.1) Загрузка инсталлятора Jira](#download-jira-installer)  
[2.2) Запуск инсталлятора Jira](#run-jira-installer)  
[2.3) Установка MySQL JDBC driver](#install-mysql-driver)  
[2.4) Продолжение установки Jira в браузере](#jira-browser-settings)  
[2.5) Установка proxy для Jira Server](#jira-proxy-settings)  
[3) Установка Confluence](#conf-installing)  
[3.1) Загрузка Инсталлятора](#download-conf-installer)  
[3.2) Запуск инсталлятора Confluence](#run-conf-installer)  
[3.3) Продолжение установки Confluence в браузере](#conf-browser-settings)  
[4) Подключение Jira к Confluence](#jira-conf-connecting)  
[5) Восстановление Jira](#jira-recovery)  
[5.1) Описание скрипта *jira_backup.sh* (установка, бэкап, восстановление)](#jira-script-desc)  
[5.2) Подключение существующей БД к JIRA](#connect-existing-db-to-jira)  
[5.3) Описание скрипта *conf_backup.sh* (установка, бэкап, восстановление)](#conf-script-desc)  
[5.4) Подключение существующей БД к CONFLUENCE](#connect-existing-db-to-conf)  
[6) Предварительные настройки и автозапуск бэкапа](#cron-start)  
[6.1) Настройка ssh](#ssh-config)  
[6.2) Автозапуск бэкапа](#run-backup)  
[6.3) Запуск скрипта проверки наличия пятничного бэкапа](#check-backup)  


**Список необходимых для установки Jira/Confluence пакетов:**

<b>Название продукта atlassian</b> | <b>Название пакета</b>  
----------------------------|------------
Jira/Confluence 			| wget,<br> expect
Jira 						| mysql-server,<br> default-jre
Confluence 					| postgresql,<br> postgresql-contrib

<a name="preparing">

### 1. Подготовительный этап.
</a>
<a name="mysql-installing">

#### 1.1. Установка mysql для Jira.
</a>

Установить mysql, выполнив команду:  
`apt install mysql-server`;  
Создайть нового пользователя БД для Jira, выполнив следующий перечень команд:  
`Mysql`    
`CREATE USER '<DBUSERNAME>'@'<HOST>' IDENTIFIED BY '<DBUSERPASSWORD>';`  
`CREATE DATABASE '<DBNAME>' CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;`  
`GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,REFERENCES,ALTER,INDEX on <DBNAME>.* TO '<DBUSERNAME>'@'<HOST>';`  
`FLUSH PRIVILEGES;`

Далее в файл */etc/mysq/mysql.conf.d/mysqld.cnf*, под строкой [mysqld], вставить следующее: 
#Установить InnoDB как хранилище по умолчанию:  
*default-storage-engine=INNODB*

#Определить кодировку, которую будет использовать сервер БД  
*character_set_server=utf8mb4*

#Установить формат строки по умолчанию как DYNAMIC:  
*innodb_default_row_format=DYNAMIC*

#Установить значение innodb_log_file_size хотя бы в 2G:  
*innodb_log_file_size=2G*

#Убедиться, что параметр sql_mode не определен как NO_AUTO_VALUE_ON_ZERO  
#удалить если существует  
#sql_mode = NO_AUTO_VALUE_ON_ZERO

#Перезапустить mysql сервер:  
`/etc/init.d/mysql stop`  
`/etc/init.d/mysql start`

Проверить наличие пользователя:  
`mysql -u root -p`

<a name="postgresql-installing">

#### 1.2 Установка PostgreSQL для Confluence
</a>

Установить необходимые пакеты:
`sudo apt update`
`sudo apt install postgresql postgresql-contrib`
Переключиться на учетную запись postgres:  
`sudo –i –u postgres`.  
Открыть терминал PostgreSQL:
`psql`
Создать пользователя бд:  
`create user <DBUSERNAME> with encrypted password '<DBUSERPASSWORD>';`.  
Создать бд для Confluence:  
`create database <DBNAME>;`  
Предоставить новому пользователю контроль над БД:  
`grant all privileges on database <DBNAME> to <DBUSERNAME>`

	

Создать файл конфигурации репозитория:  
`sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'`.  
Импортировать ключ для подписи репозитория:  
`wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add –`.  
Обновить список пакетов:  
`sudo apt-get update`.  
Установить последнюю версию PostgreSQL:  
`sudo apt-get -y install postgresql`.  
Переключиться на учетную запись postgres:  
`sudo –i –u postgres`.  
Создать пользователя бд:  
`create user <DBUSERNAME> with encrypted password '<DBUSERPASSWORD>';`.  
Создать бд для Confluence:  
`create database <DBNAME>;`  
Предоставить новому пользователю контроль над БД:  
`grant all privileges on database <DBNAME> to <DBUSERNAME>`

[К содержанию](#content)
<a name="jira-installing">

### 2 Установка Jira
</a>
<a name="download-jira-installer">

#### 2.1 Загрузка инсталлятора Jira
</a>

Скачать инсталлятор можно по [**ссылке**](https://www.atlassian.com/ru/software/jira/download-journey).  
Чтобы скачать инсталлятор jira server нужно выбрать следующие варианты:  
1. Я хочу…  
Обновить до последнего релиза  

2. Выбрать вариант развертывания  
Server ⓘ  

3. Выбрать свою версию  
Долгосрочная поддержка (8.13.8) ⓘ  

4. Выбрать свою операционную систему  
Linux 64 Bit

<a name="run-jira-installer">

#### 2.2 Запуск инсталлятора Jira
</a>

1. Перейти в папку со скачанным инсталлятором Jira и сделать его исполняемым:  
`chmod +x <ATLANSSIAN_JIRA_SOFTWARE>.bin;`  
2. Запустить инсталлятор:  
`./<ATLANSSIAN_JIRA_SOFTWARE>.bin`  
3. Далее инсталлятор запросит следующую информацию:  
Install type – выберите вторую(2) опцию(custom).  
Destination directory – куда будет установлена Jira. (Далее будет упоминаться как <JIRA_DESTINATION_DIRECTORY>) 
Home directory – где будут храниться такие данные Jira, как логи, поисковые индексы и файлы.  
TCP ports – это порт коннектора и порт управления, на которых Jira будет работать. Выберите 1(default).  
Install as service – опция доступна только при запуске инсталлятора через sudo.  
4. После завершения установки убедиться, что служба Jira запущена:  
`systemctl status jira`  
Если не запущена, запустить:  
`systemctl start jira`

**В случае возникновения ошибки запуска службы, выполнить перезагрузку, использовав команду `reboot`.**

<a name="install-mysql-driver">

#### 2.3 Установка MySQL JDBC driver
</a>

1. Открыть следующую [**ссылку**](https://dev.mysql.com/downloads/connector/j/8.0.html).  
2. В поле *Select Operating System* выбрать *Platform Independent*.  
3. Загрузить tar архив.  
4. Распаковать tar архив: `tar -xvf <DRIVER_ARCHIVE_NAME>.tar`.  
5. Перейти в папку распакованного архива. Скопировать файл *mysql-connector-java-<VERS>.jar* в 
*<JIRA_DESTINATION_DIRECTORY>/**lib*** (пункт 2.2 -> 3).  
6. Перезапустить службу Jira: `systemctl restart jira`.
<a name="jira-browser-settings">

#### 2.4 Продолжение установки Jira в браузере
</a>

1. Открыть браузер и перейти по адресу `http://localhost:8080` для продолжения установки.

>**Если при выполнении 3(п. 2.2) пункта вы установили другие порты, заменить порт 8080 на указанный вами.**  

Выбор метода установки указать *Choose I'll set it up myself.* и нажать *Next*.

2. Откроется страница *Database setup*. Выбрать опцию *Connect to your database*;

> **Если вы хотите использовать существующую БД, смотрите [пункт 5.2](#connect-existing-db-to-jira).**

Имя поля формы | Передаваемое в поле значение
---------------|--------------------------- 
Database Type | MySQL 8.0
Hostname | localhost
Port | 3306
Database | \<DBNAME\> (п. 1.1)
Username | \<DBUSERNAME\> (п. 1.1)
Password | \<DBUSERPASSWORD\> (п. 1.1)

3. Нажать на кнопку *Test Connection*. В случае успешного подключения вы увидите сообщение над формой 
*The database connection test was successful.*. 
Нажать 'кнопку' *Next* и дождаться окончания настройки БД. 
4. На странице *Set up application properties* ничего не менять:

Имя поля формы | Передаваемое в поле значение
---------------|-----------------------------
Application Title | Jira
Mode | Private
Base URL | http://localhost:8080

5. На странице *Specify your license key* у вас запросят ключ лицензии. Если у вас его нет, 
сгенерировать ключ пробной лицензии. Чтобы получить новый ключ, перейти по ссылке 
*generate a Jira trial license* под полем *Your License Key*. Вам понадобится аккаунт atlassian.
Заполнить форму следующим образом:

Имя поля формы | Передаваемое в поле значение
---------------|-----------------------------
Product | Jira Software
License type | Jira Software (Data Center)
Organization | <YOUR_ORGANIZATION_EMAIL> 
Your instance is | up and running
Server ID | не изменять

Нажмите на кнопку *Generate License*. Скопируйте сгенерированный ключ, в поле *Your License Key*  
 на странице *Specify your license key*.

6. Создать администратора Jira. Следующую страницу настроек оповещения по почте можно пропустить.  
Дождаться окончания установки.
7. Выбрать язык интерфейса.
<a name="jira-proxy-settings">

#### 2.5 Установка proxy для Jira Server
</a>

1. Открыть файл <JIRA_HOME>/bin/**setenv.sh**. Вставить в переменную *JVM_SUPPORT_RECOMMENDED_ARGS* следующую строку:
`-Dhttps.proxyHost=192.168.1.234 -Dhttps.proxyPort=3113 -Dhttp.nonProxyHost=localhost\|192.168.* -Dhttps.proxyUser=user1234 -Dhttps.proxyPassword=lab233234`
2. Сохранить изменения и перезапустите Jira: `systemctl restart jira`

[К содержанию](#content)
<a name="conf-installing">

### 3 Установка Confluence
</a>
<a name="download-conf-installer">

#### 3.1 Загрузка Инсталлятора Confluence
</a>

Скачать инсталлятор можно по [**ссылке**](https://www.atlassian.com/ru/software/confluence/download-journey)  
Чтобы скачать инсталлятор confluence server, нужно выбрать следующие варианты:  
1. Я хочу…
Обновить до последнего релиза  

2. Выбрать вариант развертывания  
Server ⓘ

3. Выбрать свою версию  
Долгосрочная поддержка (7.4.9) ⓘ

4. Выбрать свою операционную систему  
Linux 64 Bit
<a name="run-conf-installer">

#### 3.2 Запуск инсталлятора Confluence
</a>

1. Перейти в папку со скачанным инсталлятором и сделать его исполняемым:  
`chmod +x <ATLANSSIAN_CONFLUENCE>.bin;`  
2. Запустить инсталлятор:  
`./<ATLANSSIAN_CONFLUENCE>.bin`  
3. Далее инсталлятор запросит следующую информацию:  
Install type – выберать вторую(2) опцию(custom).  
Destination directory – куда будет установлен Confluence. (Далее будет упоминаться как <CONFLUENCE_DESTINATION_DIRECTORY>)  
Home directory – где будут храниться такие данные Confluence, как логи, поисковые индексы и файлы.  
TCP ports – это порт коннектора и порт управления, на которых Confluence будет работать. Выберите 1(default).  
Install as service – опция доступна только при запуске инсталлятора через sudo.  
4. После завершения установки убедиться, что служба Confluence запущена:  
`systemctl status confluence`  
Если не запущена, запустить:  
`systemctl start confluence`

**В случае возникновения ошибки запуска службы, выполнить перезагрузку, использовав команду `reboot`**

<a name="conf-browser-settings">

#### 3.3 Продолжение установки Confluence в браузере
</a>

1. Открыть браузер и перейти по адресу `http://localhost:8090` для продолжения установки.

>**Если при выполнении 3 пункта вы установили другие порты, заменить порт 8090 на указанный вами.**  

Выбор метода установки укажите *Production Installation* и нажмите *Next*.

2. Страницу *Get apps* пропустить, нажав кнопку *Next*.
3. На странице *License key* у вас запросят ключ лицензии. Если у вас его нет, сгенерировать ключ пробной лицензии.  
Чтобы получить новый ключ, перейти по ссылке *get an evaluation license* под полем *Confluence*.  
Вам понадобится аккаунт atlassian.  
Заполнить форму следующим образом:

Имя поля формы | Передаваемое в поле значение
---------------|-----------------------------
Product | Confluence
License type | Confluence (Data Center)
Organization | <YOUR_ORGANIZATION_EMAIL> 
Your instance is | up and running
Server ID | не изменять

Нажать на кнопку *Generate License*. Скопировать сгенерированный ключ в поле *Confluence* на странице *License key*.

4. На странице *Choose your deployment type* выбрать опцию *Standalone*.

5. Далее откроется страница *Set up your database*. Выбрать опцию *Connect to your database*.

Имя поля формы | Передаваемое в поле значение
---------------|--------------------------- 
Database type | PostgreSQL
Setup type | Simple
Hostname | localhost
Port | 5432
Database name | \<DBNAME\> (п. 1.2)
Username | \<DBUSERNAME\> (п. 1.2)
Password | \<DBUSERPASSWORD\> (п. 1.2)

6. На странице *Load Content* нажать на кнопку *Empty Site*.
7. На странице *Configure User Management*. Нажать на кнопку *Manage users and groups within Confluence*.
8. На странице *Configure System Administrator Account* создать администратора confluence.
9. На странице *Setup successful* нажать кнопку *Start*. Ввести имя для пространства и нажать кнопку *Continue*.

[К содержанию](#content)
<a name="jira-conf-connecting"> 

### 4. Подключение Jira к Confluence
</a>

>**Убедиться, что службы Jira и Confluence запущены.**  
>`systemctl status jira`  
>`sysyrmctl status confluence`  
>Если они не запущены, запустить:  
>`systemctl start jira`    
>`systemctl start confluence`  

1. Открыть в браузере Confluence (http://localhost:8090). Откройте созданное ранее  
пространство и нажмите на значок шестеренки в верхнем правом углу. Выберите пункт *Manage apps*.
2. Слева выберать пункт *Application Links*, который находится в блоке меню с названием *ADMINISTRATION*.
3. Ввести в поле адрес Jira: `http://localhost:8080` и нажать на кнопку *Create new link*.  
4. Далее должно появиться всплывающее окно с описанием подключаемых друг к другу приложений.  
Ничего не изменять и нажать кнопку *Continue*.
5. Вас перебросит в Jira. Там повторно появится описаное выше окно, но уже для Jira.  
Нажать *Continue*, после чего вас снова перенаправит в Confluence. 

>Над полем, куда вы вводили ссылку на приложение Jira, должно появиться сообщение *Application Link 'Jira' created successfully*.
>На текущей странице в списке приложений должно появиться приложение Jira со статусом *CONNECTED*.

[К содержанию](#content)
<a name="jira-recovery">

### 5. Восстановление Jira
</a>
<a name="jira-script-desc">

#### 5.1 Описание скрипта ***jira_backup.sh*** (установка, бэкап, восстановление)
</a>

Скрипт *jira_backup.sh* принимает следующие параметры:  
**h** - **помощь**. Данный параметр выводит список и описание доступных параметров. При использовании данного флага, другие флаги должны отсутствовать.  
**d** - **удаление**. Полное удаление jira с текущей машины (будут удалены БД и медиа файлы, добавленные пользователями).  
**i** - **установка**. Данный параметр отвечает за автоматическую установку Jira и создание для нее новой БД. Настройки после установки (создание новой [п. 1.1] или интеграция существующей [п. 5.2] БД) нужно производить самостоятельно.  
**r** - **восстановление**. Восстановление данных jira из созданного ранее бэкапа. Бэкап берется из каталога */home/jira_backup*. Если каталог будет отсутствовать или не содержать в себе архивов, выполнение скрипта прервется с ошибкой.
При наличии в каталоге нескольких архивов, данные для восстановления берутся из последнего созданного архива. 
За то, какой архив является более поздним, отвечает часть имени архива: jira_backup-**20210906130250**.tar.gz. 
Выделенное ранее число представляет собой дату и время создания архива.  
**b** - **бэкап**. Создание бэкапа jira. Данный флаг нельзя использовать с флагом *i*. В бэкап попадает следующее:  
	- файлы логов *<JIRA_SOFTWARE>/logs*  (по умолчанию <JIRA-SOFTWARE>: /opt/atlassian/jira/)
	- файлы, добавленные пользователем *<JIRA-HOME>/data* (по умолчанию <JIRA-HOME>: /var/atlassian/application-data/jira/)  
	- текущая база данных Jira  

<a name="connect-existing-db-to-jira">

#### 5.2 Подключение существующей БД к JIRA
</a>

1. Установить среду выполнения для Java (JRE), выполнив следующую команду: `apt install default-jre`.
2. Запустить инструменты конфигурации JIRA: в домашней папке JIRA (по умолчанию - */opt/atlassian/jira*), в подкаталоге *bin*
запустите скрипт *config.sh*.
4. Перейдити на вкладку *Database*. Из выпадающего списка с именем *Database type* выбрать *mysql 8.0*. Заполнить поля
следующими данными:

Имя поля | Значение
---------|---------
Hostname | localhost
Port 	 | 3306
Database | jira_db
Username | jiradbuser
Password | password

5. Нажмите на кнопку *Test connecting*. В случае успеха вы должны увидеть следующий текст: 
*Attempting to connect to the MySQL 8.0 database server...
Connection successful*.
6. Нажмите на кнопку *Save* и закройте инструмент конфигурации.
7. Перезапустите службу JIRA, выполнив следующую команду `systemctl restart jira`
8. Убедитесь, что служба работает исправно: `systemctl status jira`

> Далее нужно будет запустить переиндексацию.
9. Авторизуйтесь под пользователем с правами администратора.
10. Перейдите в *Настройки > System > Indexing* и нажмите на кнопку *Re-index*. Дождитесь окончания процедуры

<a name="conf-script-desc">

#### 5.3 Описание скрипта ***conf_backup.sh*** (установка, бэкап, восстановление)
</a>

Скрипт *conf_backup.sh* принимает следующие параметры:  
**h** - **помощь**. Данный параметр выводит список и описание доступных параметров. При использовании данного флага, другие флаги должны отсутствовать.  
**d** - **удаление**. Полное удаление confluence с текущей машины (будут удалены БД и медиа файлы, добавленные пользователями).  
**i** - **установка**. Данный параметр отвечает за автоматическую установку Confluence и создание для нее новой БД. Настройки после установки (например, создание новой [п. 3.3 - 5] БД) нужно производить самостоятельно.  
**r** - **восстановление**. Восстановление данных Confluence из созданного ранее бэкапа. Бэкап берется из каталога */home/conf_backup*. Если каталог будет отсутствовать или не содержать в себе архивов, выполнение скрипта прервется с ошибкой.
При наличии в каталоге нескольких архивов, данные для восстановления берутся из последнего созданного архива. 
За то, какой архив является более поздним, отвечает часть имени архива: conf_backup-**20210906130250**.tar.gz. 
Выделенное ранее число представляет собой дату и время создания архива.  
**b** - **бэкап**. Создание бэкапа Confluence. Данный флаг нельзя использовать с флагом *i*. В бэкап попадает следующее:  
	- файлы логов *<CONFLUENCE_SOFTWARE>/logs* (по умолчанию <CONFLUENCE-SOFTWARE>: /opt/atlassian/confluence/)
	- файлы, добавленные пользователем *<CONFLUENCE-HOME>/attachments* (по умолчанию <CONFLUENCE-HOME>: /var/atlassian/application-data/confluence/)  
	- главный конфигурационный файл *<CONFLUENCE-HOME>/confluence.cfg.xml*
	- текущая база данных Jira  

<a name="connect-existing-db-to-conf">

#### 5.4 Подключение существующей БД к CONFLUENCE
</a>

1. Остановить службу confluence, выполнив следующую команду `systemctl stop confluence`.  
2. Переключиться на учетную запись postgres:  
`sudo –i –u postgres`.  
3. Открыть терминал PostgreSQL:  
`psql`
4. Создайть пользователя бд:  
`create user <DBUSERNAME> with encrypted password '<DBUSERPASSWORD>';`.  
5. Создайть бд для Confluence:  
`create database <DBNAME>;`  
6. Предоставить новому пользователю контроль над БД:  
`grant all privileges on database <DBNAME> to <DBUSERNAME>`  
7. Извлечь БД из файла с расширением *sql* в БД:  
`<DBNAME> < <DATABASE_FILE_WITH_SQL_EXTESION>`  

>Имя новой БД (*\<DBNAME\>*) должно быть таким же, как имя БД до ее извлечения в sql файл.  
>Например, при извлечении БД в файл sql, необходимо выполнить следующую команду:  
>`pg_dump <DBNAME_BEFORE_EXTRACTING> > <SQL_BACKUP_FILE>`.  
>Исходя из вышеизложенного, имя БД *<DBNAME>* должно быть таким же, как и имя БД *<DBNAME_BEFORE_EXTRACTING>*

[К содержанию](#content)

<a name="cron-start">

### 6 Предварительные настройки и автозапуск бэкапа
</a>
<a name="ssh-config">

#### 6.1 Настройка ssh
</a>

Для автоматического копирования бэкапа на другую машину с помощью команды *scp*, необходимо провести предварительную настройку ssh.

1. Создать ssh-ключ без пароля(passphrase), выполнив следующую команду `ssh-keygen`.  
Указать путь и имя файла, который будет содержать в себе ключ.

> В скрипте указан ключ с именем id_rsa_180. При изменении имени ключа, не забыть изменить  
> его имя на свое в скриптах jira_backup.sh и conf_backup.sh.
2. Установите созданный ключ, как ключ авторизации на сервер (это нужно для выполнения команды scp без запроса пароля),  
выполнив следующую команду: `ssh-copy-id -i ~/.ssh/mykey <USER>@<HOST>`

> Пример **ssh-copy-id -i ~/.ssh/id_rsa_test root@192.168.42.180**  
3. Проверьте, не запрашивается ли пароль при попытке ssh подключения к серверу(<HOST>) с текущей машины, выполнив команду:  
`ssh -i ~/.ssh/mykey <USER>@<HOST>`

<a name="run-backup">

#### 6.2 Автозапуск бэкапа
</a>

Для автоматического создания бэкапов используется система для автоматического запуска программ и скриптов - cron.  
Порядок настройки автозапуска:  
1. Установить cron: `apt-get install cron`.  
2. Убедиться, что cron настроен для работы в фоновом режиме: `sudo systemctl enable cron`.  
3. Открыть файл crontab для редактирования: `crontab -e`.   
4. Клонировать текущий проект на машину с установленными на ней Jira/Confluence server.  
5. Переместить скрипты *jira_backup.sh* и *conf_backup.sh* в каталог */usr/local/sbin*  
6. Указать желаемые время и/или дату запуска скриптов и путь к ним.   
Формат запуска cron задачи:  

<pre>
* * * * * path_to_file [flags [ && path_to_file [flags]]]  
│ │ │ │ │  
│ │ │ │ day of week  
│ │ │ month  
│ │ day of month  
│ hour  
minute</pre>

Запуск скриптов создания бэкапа в 16:15 по пятницам, будет выглядеть следующим образом:  
`15 16 * * 5 /usr/local/sbin/jira_backup.sh b; /usr/local/sbin/conf_backup.sh b`

<a name="check-backup">

#### 6.3 Запуск скрипта проверки наличия пятничного бэкапа
</a>

1. Переместить скрипт *check_backup_existing.sh* в каталог */usr/local/sbin*.  
2. Добавить в планировщик задач следующую строку(время и дни выставить на свое усмотрение:  
в данному случае скрипт запускается в 9:10 и 17:10 с понедельника по четверг):  
`10 9,17 * * 1-4 /usr/local/sbin/check_backup_existing.sh >> /var/log/check_backup_existing.log 2>&1`  
> Логи скрипта можно посмотреть в */var/log/check_backup_existing.log*

Скрипт *check_backup_existing.sh* отвечает за проверку наличия бэкапа, сделанного в прошлую пятницу.  
При отсутствии бэкапов jira и/или confluence, сделанных в последнюю пятницу, будут запущены следующие скрипты:  
`jira_backup.sh <LAST_FRIDAY_DATE> b`   
 и/или  
`conf_backup.sh <LAST_FRIDAY_DATE> b`  
Бэкапы, полученные с помощью скрипта проверки наличия бэкапов, будут содержать в своем имени дату последней пятницы  
(*jira_backup-<LAST_FRIDAY_DATE>.tar.gz* / *conf_backup-<LAST_FRIDAY_DATE>.tar.gz*)  
**По пятницам данный скрипт не проводит проверку.**  


[К содержанию](#content)
