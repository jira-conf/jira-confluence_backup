#!/usr/bin/bash

NC='\033[0m'
CYAN='\033[0;36m'

SCRIPT_LOCATION='/usr/local/sbin/'

JIRA_BACKUP_DIR=$(grep -oP "(?<=BACKUP_DIR=).*" ${SCRIPT_LOCATION}jira_backup.sh | head -n1)
CONF_BACKUP_DIR=$(grep -oP "(?<=BACKUP_DIR=).*" ${SCRIPT_LOCATION}conf_backup.sh | head -n1)


JIRA_BACKUP_NAME=$(grep -oP "(?<=BACKUP_NAME=).*" ${SCRIPT_LOCATION}jira_backup.sh | head -n1)
CONF_BACKUP_NAME=$(grep -oP "(?<=BACKUP_NAME=).*" ${SCRIPT_LOCATION}conf_backup.sh | head -n1)


LANG=en_us_8859_1 DOW=$(date +"%a")


function check_backup() {
    BACKUP_START_DATE=$(date --date="last Friday" +%Y%m%d)
    BACKUP_LAST_FRI_DATE=$(date --date="last Friday" +%Y%m%d%H%M%S)
    JIRA_BACKUP_NAME_START_WITH=${JIRA_BACKUP_NAME}-${BACKUP_START_DATE}
    if [[ $(find ${JIRA_BACKUP_DIR} -type f -name "${JIRA_BACKUP_NAME_START_WITH}*" 2> /dev/null) ]];then
	echo -e "[$(date +%d.%m.%Y_%H:%M)] ${CYAN}JIRA${NC}: LAST FRIDAY ARCHIVE ARCHIVE WAS FOUNDED!"
    else
	${SCRIPT_LOCATION}jira_backup.sh ${BACKUP_LAST_FRI_DATE} b
    fi
    CONF_BACKUP_NAME_START_WITH=${CONF_BACKUP_NAME}-${BACKUP_START_DATE}
    if [[ $(find ${CONF_BACKUP_DIR} -type f -name "${CONF_BACKUP_NAME_START_WITH}*" 2> /dev/null) ]];then
	echo -e "[$(date +%d.%m.%Y_%H:%M)] ${CYAN}CONFLUENCE${NC}: LAST FRIDAY ARCHIVE ARCHIVE WAS FOUNDED!"
    else
	${SCRIPT_LOCATION}conf_backup.sh ${BACKUP_LAST_FRI_DATE} b
    fi
}

if [ "${DOW}" != "Fri" ];then
    check_backup
fi
