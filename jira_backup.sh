#!/usr/bin/bash

PCOL='\033[1;35m'
BOLD='\033[1m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
RED='\033[1;31m'
NC='\033[0m'
CYAN='\033[0;36m'
ERR="${RED}[E]${NC} "
WAR="${YELLOW}[W]${NC} "
OK="${GREEN}[OK]${NC} "
INF="${CYAN}[I]${NC} "

###########JIRA_INSTALL_VARS#################

#JIRA_INSTALL_PATH=/opt/atlassian/jira
JIRA_SOFTWARE_DATA=/var/atlassian/application-data/jira
CONNECTOR_NAME="mysql-connector.tar.gz"
TEMP_DIR="temp_conn_dir"
JIRA_VER="8.13.8"
JIRA_NAME="jira-${JIRA_VER}.bin"

#############CREATE_BACKUP_VARS#################

function check_first_param() {
    if [ $# -eq 0 ];then
	echo -e "${ERR}There is no arguments! You must pass at least one"
	echo -e "${INF}Example of usage: ./jira_backup.sh h [d i r b]"
	exit 1
    else
	SIZE=${#1}
	if [ $SIZE -eq 14 ];then
		if [[ $1 =~ ^[0-9]+$ ]];then
	        echo -e "${INF}Archive file will be build with last friday's date in name\n"
	        BACKUP_DATE=$1
		else
		    echo -e "${ERR}First argument contains not only digits.".
		    exit 1
		fi
	else
	    BACKUP_DATE=$(date +%Y%m%d%H%M%S)
	fi
fi
}

check_first_param $1

JIRA_DB_NAME=jira_db
BACKUP_DIR=/home/jira_backup
BACKUP_DB_NAME=jira_db
#BACKUP_DATE=$(date +%Y%m%d%H%M%S)
SQL_BACKUP_FILE=${BACKUP_DIR}/${BACKUP_DB_NAME}-${BACKUP_DATE}.sql
JIRA_HOME=/opt/atlassian/jira
BACKUP_ARCHIVE_NAME=jira_backup-${BACKUP_DATE}.tar.gz
SCP_BACKUP_DIR=/home/jira_backup
SCP_ADDRESS='192.168.42.180'
#SCP_PASS='password'
#JIRA_DATA_PATH=/var/atlassian/application-data/jira

#############JIRA_RECOVERY_VARS##################

REG_BACKUP_DIR="\/home\/jira_backup"
#BACKUP_DIR_CLEAR=$(echo $BACKUP_DIR | sed -e 's/\\//g')
BACKUP_NAME=jira_backup
JIRA_DATA_DIR=${JIRA_SOFTWARE_DATA}/data
#JIRA_DATA_SOFT=/var/atlassian/application-data/jira
#JIRA_HOME=/opt/atlassian/jira
if [ ! -d "${BACKUP_DIR}" ];then 
    echo -e "${ERR}Backup dir(${BACKUP_DIR}) doesn't exist";exit 1;
fi
for entry in "${BACKUP_DIR}"/*
do
    LAST=$(echo $entry | sed -e "s/${REG_BACKUP_DIR}\/${BACKUP_NAME}-//g")
    IFS=' ' read -r -a array <<< $(echo $LAST | sed -e "s/.tar.gz//g")
done
max=${array[0]}
ROOT='root'
ROOT_PASS='password'
#JIRA_DB_NAME='jira_db'
JIRA_DB_USERNAME='jiradbuser'
JIRA_DB_USER_PASS='password'
JIRA_USER_HOST='localhost'

#############BACKUP_FUNCTION############

function createBackup {


mysqldump -u${ROOT} -p${ROOT_PASS} ${JIRA_DB_NAME} --result-file=${SQL_BACKUP_FILE}
##block below doesn't wrok with contab
#/usr/bin/expect <(cat << EOF
#spawn mysqldump -uroot -p ${JIRA_DB_NAME} --result-file=${SQL_BACKUP_FILE};\
#expect "*password*";\
#send "password\r";\
#interact
#EOF
#)

if [ ! -d "${BACKUP_DIR}" ];then
    mkdir -p ${BACKUP_DIR}
fi

if [ ! -d "${JIRA_HOME}/logs" ];then
    echo -e "${WAR}Can't find logs dir in ${JIRA_HOME}/";
    #exit 1;
fi

cd ${BACKUP_DIR}
tar -cvf ${BACKUP_ARCHIVE_NAME} ${BACKUP_DB_NAME}-${BACKUP_DATE}.sql
cd ${JIRA_HOME}/
tar -rvf ${BACKUP_DIR}/${BACKUP_ARCHIVE_NAME} logs
rm -rf ${SQL_BACKUP_FILE}
#echo -e "${BACKUP_DIR1}${BACKUP_ARCHIVE_NAME}"

cd ${JIRA_SOFTWARE_DATA}
tar -rvf ${BACKUP_DIR}/${BACKUP_ARCHIVE_NAME} data

scp -i ~/.ssh/id_rsa_180 ${BACKUP_DIR}/${BACKUP_ARCHIVE_NAME} root@${SCP_ADDRESS}:${SCP_BACKUP_DIR}
#/usr/bin/expect <(cat << EOF
#set timeout 120
#spawn scp ${BACKUP_DIR}/${BACKUP_ARCHIVE_NAME} root@${SCP_ADDRESS}:${SCP_BACKUP_DIR}
#expect "*password*"
#send "${SCP_PASS}\r"
#interact
#EOF
#)

}

#createBackup
###################RECOVERY FUNCTION###############################

function dbRecovery(){

TAR_COUNT=$(ls -1 ${BACKUP_DIR}/*.tar.gz 2>/dev/null | wc -l)
if [ $TAR_COUNT != 0 ];then
    find ${BACKUP_DIR} -type f ! -name '*.tar.gz' -delete
    rm -R -- ${BACKUP_DIR}/*/
else
    echo -e "${ERR}Can't find any '.tar.gz' files in ${BACKUP_DIR}";exit 1
fi

function import_db(){
    systemctl start mysql
    MYSQL_CONF=/etc/mysql/mysql.conf.d/mysqld.cnf
    sed -i 's/sql_mode = NO_AUTO_VALUE_ON_ZERO//' ${MYSQL_CONF}
    sed -i 's/default-storage-engine=INNODB//' ${MYSQL_CONF}
    sed -i 's/character_set_server=utf8mb4//' ${MYSQL_CONF}
    sed -i 's/innodb_default_row_format=DYNAMIC//' ${MYSQL_CONF}
    sed -i 's/innodb_log_file_size=2G//' ${MYSQL_CONF}
    sed -i '/^\[mysqld\]/a default-storage-engine=INNODB\ncharacter_set_server=utf8mb4\ninnodb_default_row_format=DYNAMIC\ninnodb_log_file_size=2G' ${MYSQL_CONF}
    systemctl restart mysql

    mysql -u $ROOT -p$ROOT_PASS -Bse "INSTALL PLUGIN validate_password SONAME 'validate_password.so';" > /dev/null 2>&1
    mysql -u $ROOT -p$ROOT_PASS -Bse "\
    SET GLOBAL validate_password_policy = LOW;\
    SET GLOBAL validate_password_special_char_count = 0;\
    CREATE USER IF NOT EXISTS '${JIRA_DB_USERNAME}'@'${JIRA_USER_HOST}' IDENTIFIED BY '${JIRA_DB_USER_PASS}';\
    DROP DATABASE IF EXISTS ${JIRA_DB_NAME};\
    CREATE DATABASE ${JIRA_DB_NAME};\
    GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,REFERENCES,ALTER,INDEX on ${JIRA_DB_NAME}.* TO '${JIRA_DB_USERNAME}'@'${JIRA_USER_HOST}';\
    FLUSH PRIVILEGES;"
    mysql -u $USER -p${ROOT_PASS} "${JIRA_DB_NAME}" < "$1"
}

for element in "${array[@]}"
do
    if [ $max -lt $element ];then
	#echo "MAX IS $element"
	max=$element
    fi
    if [ "$element" = "${array[${#array[@]}-1]}" ];then
	#rm -f *.sql
	tar -xvf "${BACKUP_DIR}/${BACKUP_NAME}-${element}.tar.gz"
	SQL_FILE_NAME="jira_db-${max}.sql"
	ls
	if [ -d "${JIRA_HOME}/logs" ];then
	    chown -R jira:jira logs
	    cp -Rf logs/* ${JIRA_HOME}/logs
	else
	    echo -e "${INF}There is no Jira's log directory in ${JIRA_HOME}"
	fi
	if [ -d "${JIRA_DATA_DIR}" ];then
	    rm -r ${JIRA_DATA_DIR}/* > /dev/null 2>&1
	    cp -R data/* ${JIRA_DATA_DIR}
	    chown -R jira:jira ${JIRA_SOFTWARE_DATA}
	else
	    echo -e "${INF}There is no Jira's data directory in ${PCOL}${JIRA_DATA_DIR}${NC}"
	    #mkdir -p ${JIRA_DATA_DIR}
	    install -d -m 0755 -o jira -g jira ${JIRA_DATA_DIR}
	    cp -R data/* ${JIRA_DATA_DIR}
	    chown -R jira:jira ${JIRA_SOFTWARE_DATA}
	fi
	import_db ${SQL_FILE_NAME}
	rm -f ${SQL_FILE_NAME}
    fi
done
rm -rf logs
rm -rf data

}
#####################RECOVERY_FUNCTION_END########################

#####################REMOVE_FUNCTION#######################

function removeJira {

#if [ "$1" = "d" ];then
    mysql -u $ROOT -p$ROOT_PASS -Bse "\
    DROP DATABASE IF EXISTS ${JIRA_DB_NAME};"
    mysql -u $ROOT -p$ROOT_PASS -Bse "\
    DROP USER '${JIRA_DB_USERNAME}'@'${JIRA_USER_HOST}';" 2> /dev/null
    systemctl stop jira
    echo -e "y" | ${JIRA_HOME}/uninstall
    if [ -d "${JIRA_HOME}/logs" ];then rm -rf ${JIRA_HOME}/logs;fi;
    if [ -d "${JIRA_HOME}/lib" ];then rm -rf ${JIRA_HOME}/lib;fi;
    rm -rf ${JIRA_SOFTWARE_DATA}
    echo -e "${INF}Jira was removed!"
    #exit 1
#fi

}

#######################REMOVE_FUNCTION_END#######################
####################FUNCTION_WGET_PROXY#################

function setWGETproxy {

WGETRC="/etc/wgetrc"
PROXY="http:\/\/user1234:lab233234@192\.168\.1\.234:3113\/"
STR1="#https_proxy = http:\/\/proxy\.yoyodyne\.com:18023\/"
STR2="#http_proxy = http:\/\/proxy\.yoyodyne\.com:18023\/"
STR3="#use_proxy = on"
EXT1="http_proxy = http://user1234:lab233234@192.168.1.234:3113/"
EXT2="https_proxy = http://user1234:lab233234@192.168.1.234:3113/"
if grep -Fxq "$EXT1" "${WGETRC}"
then
echo -e "${OK}FOUND ${EXT1}!";
else
sed -i "s/${STR2}/http_proxy = ${PROXY}/" "${WGETRC}"
fi
if grep -Fxq "$EXT2" "${WGETRC}"
then
echo -e "${OK}FOUND ${EXT2}!";
else
sed -i "s/${STR1}/https_proxy = ${PROXY}/" "${WGETRC}"
fi
if grep -Fxq "use_proxy = on" "${WGETRC}"
then
echo -e "${OK}FOUND use_proxy = on!";
else
sed -i "s/${STR3}/use_proxy = on/" "${WGETRC}"
fi

}

#####################FUNCTION_WGET_PROXY_END################


#####################INSTALL_FUNCTION#######################

function installJira {

setWGETproxy

if [ ! -f "${JIRA_NAME}" ];then
    wget -O ${JIRA_NAME} https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-${JIRA_VER}-x64.bin
else
    echo -e "${INF}Installer of Jira (${JIRA_NAME}) was founded in current folder ${PCOL}$(pwd)${NC}"
    echo -e "${INF}Jira will be installed from this file (${PCOL}${JIRA_NAME}${NC})!"
fi
chmod a+x ${JIRA_NAME}



echo -e "${INF}Stating to install Jira..."
echo -e "o\n2\n${JIRA_HOME}\n${JIRA_SOFTWARE_DATA}\n1\ny\ni" | ./${JIRA_NAME}
FILE="${JIRA_HOME}/bin/setenv.sh"
sed -i 's/JVM_SUPPORT_RECOMMENDED_ARGS=\"\"/JVM_SUPPORT_RECOMMENDED_ARGS=\"-Dhttps.proxyHost=192.168.1.234 -Dhttps.proxyPort=3113 -Dhttp.nonProxyHost=localhost\\|192.168.* -Dhttps.proxyUser=user1234 -Dhttps.proxyPassword=lab233234\"/' $FILE

wget -O ${CONNECTOR_NAME} https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-8.0.26.tar.gz
if [ ! -d "${TEMP_DIR}" ];then
    mkdir ${TEMP_DIR};
elif [ -d ${TEMP_DIR} ];then
    rm -rf ${TEMP_DIR}
    mkdir ${TEMP_DIR}
fi

tar -xvf ${CONNECTOR_NAME} -C ${TEMP_DIR}
set -- ${TEMP_DIR}/*
DIR_NAME=$1
ARCH_DIR_NAME=$(echo ${DIR_NAME} | sed -E "s/^${TEMP_DIR}\///")
#echo ${ARCH_DIR_NAME}


cp ${TEMP_DIR}/${ARCH_DIR_NAME}/${ARCH_DIR_NAME}.jar ${JIRA_HOME}/lib
systemctl restart jira
#systemctl status jira

rm -rf ${TEMP_DIR}
rm -f ${CONNECTOR_NAME}

echo -e "${INF}Open this link in your browser: localhost:8080 "

}


#####################INSTALL_FUNCTION_END######################

#if [[ "$@" == "" ]];then
#    echo -e "${ERR}There is no arguments! You must pass at least one"
#    echo -e "${INF}Example of usage: ./jira_backup.sh h [d i r b]"
#    exit 1
#fi
h_flag=0
i_flag=0
#d_flag=0

TMP_FILE=/tmp/tmp_file

if [ ! -f "${TMP_FILE}" ];then touch "${TMP_FILE}";else rm -f ${TMP_FILE};touch "${TMP_FILE}";fi;

for var in "$@"
do
#    echo "VAR: "$var
    if [ "$var" = "h" ];then echo "h_flag=1" >> "${TMP_FILE}";fi
    if [ "$var" = "i" ];then echo "i_flag=1" >> "${TMP_FILE}";fi
#    if [ "$var" = "d" ];then echo "d_flag=1" >> "${TMP_FILE}";fi

done

h_flag=$(echo $(sed -n "/^h_flag=*/p" "${TMP_FILE}") | sed "s/^h_flag=//")
i_flag=$(echo $(sed -n "/^i_flag=*/p" "${TMP_FILE}") | sed "s/^i_flag=//")
#d_flag=$(echo $(sed -n "/^d_flag=*/p" "${TMP_FILE}") | sed "s/^d_flag=//")

if [ "${h_flag}" = "" ];then h_flag=0;fi
if [ "${i_flag}" = "" ];then i_flag=0;fi
#if [ "${d_flag}" = "" ];then d_flag=0;fi



function h_check(){
    #echo $1
    if [ "$1" = "1" ];then echo -e "${WAR}h flag must be used without others flags ";exit 1;fi;
}

for var in "$@"
do
    
    if [ "$var" = "h" ];then
	
	echo -e "\
	${INF}List of flags:\n\
	${INF}${BOLD}d${NC} - remove Jira;\n\
	${INF}${BOLD}i${NC} - install Jira;\n\
	${INF}${BOLD}r${NC} - recovery Jira data from backup;\n\
	${INF}${BOLD}b${NC} - backup Jira data. Backup includes next: \n\
	\t${PCOL}/opt/atlassian/jira/logs${NC},\n\
	\t${PCOL}/var/atlassian/application-data/jira/data${NC},\n\
	\tcurrent Jira's database."
	h_flag=1
    fi
    if [ "$var" = "d" ];then
	h_check $h_flag
	echo -e "${INF}Removing Jira..."
	#d_flag=1
	#remove
	removeJira
	
    fi
    if [ "$var" = "i" ];then
	h_check $h_flag
	echo -e "${INF}Installing Jira..."
	i_flag=1
	#install
	installJira
	
    fi
    if [ "$var" = "r" ];then
	h_check $h_flag
	echo -e "${INF}Recovering Jira's data"
	#recovery
	dbRecovery
    fi
    if [ "$var" = "b" ];then
	h_check $h_flag
	if [ "${i_flag}" = "1" ];then
	    echo -e "${ERR}There is no reason to create empty backup!\n${ERR}Don't use b flag with 'i' flag";
	    exit 1
	fi
	#backup
	echo -e "${INF}Creating backup..."
	createBackup
	
    fi
    #echo "$var"
    
done